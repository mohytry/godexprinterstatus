import java.util.NoSuchElementException;
import java.util.Scanner;

public class GodexPrinterStatusMain
{
    public static void main(String[] args)
    {
        //getPrinterStatus();
        //getPrinterStatus2();
        //getConfig2();
        //getConfig();
        //testActiveResponse();
        //resetPrinter();
        //calibrateAndGetStatusConfig();


        //calibrateWithActiveResponse();//make sure to bufClr beforehand
        //getPrinterStatus();
        //getPrinterStatus2();

        testActiveResponse();
        /*try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }*/
        //interrupt print after 1 second
        System.out.println("end");

    }

    public static String getPrinterStatus()
    {
        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("^XSET,IMMEDIATE,1");//set immediate respone as prerequisit for CHECK
        printer.sendcommand("~S,CHECK");
        String data = printer.readPrinterResponses(true);
        System.out.println(data);
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("status check took millis: " + durationMillis);
        printer.disconnectPrinter();
        return data;
    }

    public static String getPrinterStatus2()
    {
        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("^XSET,IMMEDIATE,1");//set immediate respone as prerequisit for CHECK
        printer.sendcommand("~S,CHECK");
        byte[] out = new byte[2048];
        String data = printer.RcvBuf(out);
        System.out.println(data);
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("status check took millis: " + durationMillis);
        printer.disconnectPrinter();
        return data;
    }

    public static String getConfig2()
    {
        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("^XGET,CONFIG");
        byte[] out = new byte[10000];
        String data = printer.readPrinterResponses();
        System.out.println(data);
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("status check took millis: " + durationMillis);
        printer.disconnectPrinter();
        return data;
    }

    public static String getConfig()
    {
        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("^XGET,CONFIG");
        String data2 = printer.readPrinterResponses();
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("getConfig took millis: " + durationMillis);
        printer.disconnectPrinter();
        return data2;
    }

    public static String calibrateAndGetStatusConfig()
    {
        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("~S,SENSOR");
        String data = "";
        try
        {
            Thread.sleep(1000);//initial sleep so check doesnt get respojnse before printer becomes unresponsive due to calibration
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        //data = printer.pollPrinterCmdResponse("~S,CHECK", true);
        while (data != null && !data.contains("00"))//not idle/ready //todo: add timeout
        {
            data = printer.pollPrinterCmdResponse("~S,CHECK", true);
            System.out.println("polled for data: " + data);
        }
        System.out.println("-> ready state reached");
        System.out.println("time passsed since calibration start: " + (System.currentTimeMillis() - l) + "\r\n");

        System.out.println("polling for config data...");
        printer.sendcommand("^XGET,CONFIG"); //SOMETIMES RETURNS  CONFIG DATA two times?
        String data2 = printer.readPrinterResponses();//|| readPrinterResponses(3) to skip last RcvBuf that takes 900ms due to timeout
        System.out.println(data2);

        if (data2.contains("^Q100,0,0"))
        {
            System.out.println("detected continuous Material (no gaps)");
        }
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("calibration and getConfig took millis: " + durationMillis);
        printer.disconnectPrinter();
        return data;
    }

    public static String calibrateWithActiveResponse()
    {
        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("~S,BUFCLR");
        printer.sendcommand("^XSET,IMMEDIATE,1");
        printer.sendcommand("^XSET,ACTIVERESPONSE,1");
        printer.sendcommand("~S,SENSOR");
        String data = "";
        try
        {
            Thread.sleep(2000);//initial timeout, keeps check from completing BEFORE printer does reset
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        System.out.println("polling start");
        data = printer.readPrinterResponses();
        System.out.println("polling end");
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("duration millis: " + durationMillis);
        printer.disconnectPrinter();
        return data;
    }

    public static void testActiveResponse()
    {
        //goLabel composed simple label(11,50)mm with a rectaangle //Q50,3//H5
        String simpleRectLabel = "^Q150,0,0\n" +
                "^W11\n" +
                "^H10\n" +
                "^P1\n" +
                "^S2\n" +
                "^AD\n" +
                "^C1\n" +
                "^R0\n" +
                "~Q+0\n" +
                "^O0\n" +
                "^D0\n" +
                "^E12\n" +
                "~R255\n" +
                "^XSET,ROTATION,0\n" +
                "^L\n" +
                "Dy2-me-dd\n" +
                "Th:m:s\n" +
                "R6,8,77,1110,4,4\n" +
                "E\n";
        Scanner scanner = new Scanner(simpleRectLabel);
        scanner.useDelimiter("\n");

        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("~S,BUFCLR"); //clear buffer before starting anything
        printer.sendcommand("^XSET,IMMEDIATE,1"); //necessary for print interrupt commands
        printer.sendcommand("^XSET,ACTIVERESPONSE,0"); //not necessary here, doesn't hurt either
        //now active response is toggled on -> so do some action that would cause a resonse (e.g. open printer)
        //do this within ca. 5 seconds else polling times out
        //now wait for response, usually do this on a background thread


        //EDIT without another printer action the printer won't give any feedback so instruct a reset/calibration or similar to elicit an error code
        //printer.sendcommand("~S,SENSOR"); //causes output but no error message even with open printer and no material in
        //printer.sendcommand("~Z");//causes 09 (syntax error???) because printer is resetting

        //sending commands
        while (true)
        {
            try
            {
                String nextCommand = scanner.next(); //now recognizes no paper (with printer open) 01, open but with paper gives ERROR09
                System.out.println("sending command: " + nextCommand);
                printer.sendcommand(nextCommand);
            }
            catch (NoSuchElementException ex)
            {
                //no more commands so stop sending and continue with polling
                break;
            }
        }
        String data = "";

        //start polling
        System.out.println("start polling start");
        while (data.isEmpty() || data.startsWith("50"))
        {
            data = printer.pollPrinterCmdResponse("~S,CHECK", true, 10000);
            System.out.println(data);
            if ((System.currentTimeMillis() - l) > 1500 && data.startsWith("50"))
            {
                System.out.println("send print interrupt and get back to ready mode");
                printer.sendcommand("~S,PAUSE");
                printer.sendcommand("~S,CANCEL");
                printer.sendcommand("~S,BUFCLR");
                System.out.println("check status");
                data = printer.pollPrinterCmdResponse("~S,CHECK", true);
                System.out.println("return status: "+data);
                break;
            }
        }
        System.out.println("polling end");
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("duration millis: " + durationMillis);

        if (data.contains("ERROR09"))//caused by open door, 2009-110 paper in
        {
            System.out.println("now try to reset printer");
            printer.sendcommand("~S,CANCEL");
            //seem to work but now printer wont calibrate anymore (via command from GoDoctor or calibrateAndGetStatusConfig())
            //will calibrate from hardware button at back of printer
            //goDoc Auto-Calibrate prints "Auto-Sensing Fail\n No Data Return"
            //tried resetting (works but still no working auto-calib, tried clearing all memory, tried turning on and off again (with removing power cord))
        }

        printer.disconnectPrinter();
    }

    //reset printer and get status (usually idle) when done
    public static String resetPrinter()
    {
        WagoPrinter printer = WagoPrinter.getFirstAvailablePrinter();
        printer.connectPrinter();
        long l = System.currentTimeMillis();
        printer.sendcommand("~Z");
        String data = "";
        System.out.println("start sleeping for reset");

        try
        {
            Thread.sleep(4000);//initial timeout, keeps check from completing BEFORE printer does reset
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        WagoPrinter printer2 = null;
        while (data.isEmpty())
        {
            printer2 = WagoPrinter.getFirstAvailablePrinter();//keep trying to connect
            if (printer2 != null)
            {
                printer2.connectPrinter();
                data = printer2.pollPrinterCmdResponse("~S,CHECK", true);
            }
        }

        System.out.println(data);
        long durationMillis = (System.currentTimeMillis() - l);
        System.out.println("reset + check took millis: " + durationMillis);
        printer2.disconnectPrinter();
        return data;
    }
}
