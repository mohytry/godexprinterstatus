import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class WagoPrinter
{
    static EZioLib.API API = EZioLib.getInstance();
    private String printerUsbId;

    private WagoPrinter(String usbID)
    {
        this.printerUsbId = usbID;
    }

    public static WagoPrinter getFirstAvailablePrinter()
    {
        List<WagoPrinter> allAvailablePrinters = getAllUSBPrinters();
        if (allAvailablePrinters.size() > 0)
        {
            return allAvailablePrinters.get(0);
        }
        return null;
    }

    public static List<WagoPrinter> getAllUSBPrinters()
    {
        List<String> allUsbIDs = getAllUsbIDs();
        List<WagoPrinter> printerList = new ArrayList<>();
        for (String usbID : allUsbIDs)
        {
            printerList.add(new WagoPrinter(usbID));
        }
        return printerList;
    }

    private static List<String> getAllUsbIDs()
    {
        List<String> wagoPrinters = new ArrayList<>();

        byte[] id = new byte[64];
        int i = API.FindFirstUSB(id);
        if (i == 1)
        {
            wagoPrinters.add(new String(id));
        }
        else
        {
            System.out.println("failed to find first USB ID for printer!");
        }
        while (i == 1)
        {
            i = API.FindNextUSB(id);
            if (i == 1)
            {
                wagoPrinters.add(new String(id));
            }
        }
        return wagoPrinters;
    }

    public void connectPrinter()
    {
        API.OpenUSB(this.printerUsbId);
    }

    public void disconnectPrinter()
    {
        API.closeport();
    }

    public void sendcommand(String command)
    {
        int i = API.sendcommand(command);
    }

    public String readPrinterResponses()
    {
        return readPrinterResponses(false);
    }

    public String readPrinterResponses(boolean breakOnFirstRcv)
    {
        byte[] out = new byte[2048];
        int result = -1;
        int maxRetry = 5;
        int curTry = 0;
        String data = "";
        Charset ascii = Charset.forName("US-ASCII");
        while (curTry < maxRetry)
        {
            result = API.RcvBuf(out, out.length);
            if (result == 0)
            {
                curTry++;
                if(!data.isEmpty())
                {
                    break;
                }
            }
            else
            {
                curTry = 0;
                String newStr = new String(out, ascii);
                data += newStr;
                if(newStr.isEmpty() || (!data.isEmpty() && breakOnFirstRcv))
                {
                    break;
                }
            }
            //clear by realoc
            out = new byte[2048];
        }
        System.out.println("retData: " + data);
        return data;
    }

    public String readPrinterResponses(int readAfterFirstRcv)
    {
        byte[] out = new byte[2048];
        int result = -1;
        int maxRetry = 5;
        int curTry = 0;
        String data = "";
        Charset ascii = Charset.forName("US-ASCII");
        while (curTry < maxRetry)
        {
            result = API.RcvBuf(out, out.length);
            if (result == 0)
            {
                curTry++;
                if (!data.isEmpty())
                {
                    break;
                }
            }
            else
            {
                curTry = 0;
                String newStr = new String(out, ascii);
                data += newStr;
                for (int i = 0; i < readAfterFirstRcv - 1; i++)
                {
                    API.RcvBuf(out, out.length);
                    data += new String(out, ascii);
                }
                break;
            }
        }
        return data;
    }

    public String pollPrinterCmdResponse(String stringCommand, boolean breakOnFirstRcv,double timeoutMillis)
    {
        byte[] out = new byte[2048];
        int result = -1;
        String data = "";
        Charset ascii = Charset.forName("US-ASCII");
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < timeoutMillis)
        {
            result = API.RcvBuf(out, out.length);
            if (result == 0)
            {
                API.sendcommand(stringCommand);
                if(!data.isEmpty())
                {
                    break;
                }
            }
            else
            {
                String newStr = new String(out, ascii);
                data += newStr;
                if(newStr.isEmpty() || (!data.isEmpty() && breakOnFirstRcv))
                {
                    break;
                }
            }
            //clear by realoc
            out = new byte[2048];
        }
        return data;
    }

    public String pollPrinterCmdResponse(String stringCommand,boolean breakOnFirstRcv)
    {
        byte[] out = new byte[2048];
        int result = -1;
        int maxRetry = 5;
        int curTry = 0;
        String data = "";
        Charset ascii = Charset.forName("US-ASCII");
        while (curTry < maxRetry)
        {
            result = API.RcvBuf(out, out.length);
            if (result == 0)
            {
                API.sendcommand(stringCommand);
                curTry++;
                if(!data.isEmpty())
                {
                    break;
                }
            }
            else
            {
                curTry = 0;
                String newStr = new String(out, ascii);
                data += newStr;
                if(newStr.isEmpty() || (!data.isEmpty() && breakOnFirstRcv))
                {
                    break;
                }
            }
            //clear by realoc
            out = new byte[2048];
        }
        //System.out.println("retData: " + data);
        return data;
    }

    public String RcvBuf(byte[] outBuf)
    {
        String data = "";
        Charset ascii = Charset.forName("US-ASCII");
        API.RcvBuf(outBuf, outBuf.length);
        data += new String(outBuf, ascii);
        API.RcvBuf(outBuf, outBuf.length);
        data += new String(outBuf, ascii);
        API.RcvBuf(outBuf, outBuf.length);
        data += new String(outBuf, ascii);
        return data;
    }
}
